package com.antra.raghu;

public class Test {
	public static boolean isPalindrome(String str) {
		if(str ==null || str.trim().length() == 0)
			return false;

		char[] arr = str.toCharArray();
		int len = arr.length;
		for(int i=0;i<len/2;i++) {
			if(arr[i] != arr[len-i-1])
				return false;
		}
		return true;
	}
	public static void main(String[] args) {
		String input ="TESTSET";
		boolean valid = isPalindrome(input);
		System.out.println(valid);
	}
}








