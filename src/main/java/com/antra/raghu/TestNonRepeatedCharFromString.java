package com.antra.raghu;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Find The First Non Repeated Character In A String
 * 
 * @author Shettem Raghavendra
 *
 */
public class TestNonRepeatedCharFromString {

	public static void main(String[] args) {
		String str = "HeldloTesttrH";

		usingIndexConcept(str);
		usingMapConcept(str);
	}

	private static void usingMapConcept(String str) {
		Map<Character, Integer> map = new LinkedHashMap<>();
		char[] arr = str.toCharArray();
		for (char c : arr) {
			//short
			map.put(c, map.containsKey(c) ? map.get(c) + 1 : 1);
			//long
			/*if (map.containsKey(a)) {
				map.put(a, map.get(a) + 1);
			} else {
				map.put(a, 1);
			}*/
		}
		/*//old
		 * for(Entry<Character,Integer> entry : map.entrySet()) {
		 * if(entry.getValue()==1) { System.out.println(entry.getKey()); break; } }
		 */
		//java 8
		char c = map.entrySet().stream()
				.filter(e -> e.getValue() == 1)
				.findFirst()
				.get().getKey();
		System.out.println(c);

	}

	private static void usingIndexConcept(String str) {
		char[] arr = str.toCharArray();
		for (char i : arr) {
			if (str.indexOf(i) == str.lastIndexOf(i)) {
				System.out.println(i);
				break;
			}
		}
	}
}
