package com.antra.raghu;

public class QuickSortEx {

	private static void swap(int[] arr,int i,int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	private static int partitions(int[] arr,int low,int high) {
		int pivotal = arr[high];
		int i = low - 1;
		for(int j = low ; j < high ; j++) {
			if(arr[j] < pivotal) {
				i++;
				swap(arr,i,j);
			}

		}
		swap(arr, i+1, high);
		return i+1;
	}
	public static void quicksort(int[] arr,int low, int high) {
		if(low < high) {
			int pi = partitions(arr, low, high);
			quicksort(arr, low, pi-1);
			quicksort(arr, pi+1, high);
		}
	}
	public static void main(String[] args) {
		int[] arr = {98,11,23,56,87,94,9,76};
		quicksort(arr, 0, arr.length-1);
		for(int temp : arr) {
			System.out.println(temp);
		}
	}
}
