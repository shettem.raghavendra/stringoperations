package com.antra.raghu;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestWordsCount {

	public static void main(String[] args) {
		String s = "Hello Users How are you Welcome Hello Users are you there";
		String[] arr = s.split(" ");
		Map<String,Long> output = 
				Arrays.asList(arr)
				.stream()
				.collect(Collectors.groupingBy(
						Function.identity(),Collectors.counting()));
		System.out.println(output);
	}
}
