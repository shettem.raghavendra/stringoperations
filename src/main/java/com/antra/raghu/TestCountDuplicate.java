package com.antra.raghu;

import java.util.HashMap;
import java.util.Map;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

/**
 * Java Program to Count Duplicate Characters in a String 
 * (or) count no.of times  character occurred in String
 * 
 * @author Shettem Raghavendra
 *
 */
public class TestCountDuplicate {

	public static void main(String[] args) {
		String str = "HelloHelloHHabcd";
		Map<Character, Integer> countMap1 = countByMap(str);
		Map<Character, Long> countMap2 = countByStreamApi(str);
		System.out.println(countMap1);
		//print only duplicates
		countMap2.entrySet().stream()
		//.filter(e -> e.getValue() > 1)
		.forEach(System.out::println);
	}

	private static Map<Character, Long> countByStreamApi(String str) {
		return str.chars()
				.mapToObj(c->(char)c)
				.collect(
						Collectors.groupingBy(UnaryOperator.identity(),Collectors.counting())
						);
	}

	private static Map<Character, Integer> countByMap(String str) {
		Map<Character, Integer> map = new HashMap<>();

		char[] arr = str.toCharArray();
		for (Character c : arr) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
			} else {
				map.put(c, 1);
			}
		}
		return map;
	}
}
