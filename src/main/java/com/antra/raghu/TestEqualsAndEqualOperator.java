package com.antra.raghu;

public class TestEqualsAndEqualOperator {

	public static void main(String[] args) {
		String s1 = "Raghu";
		String s2 = "Raghu";
		System.out.println(s1==s2);//are they referring same object?
		System.out.println(s1.equals(s2)); // is data same?
		
		String s3 = new String("Raghu");
		String s4 = new String("Raghu");
		System.out.println(s3==s4);
		System.out.println(s3.equals(s4));
	}
}
