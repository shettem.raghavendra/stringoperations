package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

public class TestReverseWordInString {

	public static void main(String[] args) {
		String s = "Hello Users How are you Welcome Hello Users are you there";
		String[] arr = s.split(" ");
		List<String> output = 
				Arrays.asList(arr)
				.stream()
				.map(str -> new StringBuilder(str).reverse().toString())
				.toList();
		System.out.println(String.join(" ", output));
	}
}
