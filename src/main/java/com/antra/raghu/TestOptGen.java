package com.antra.raghu;

import java.util.Random;

/***
 * Logic to Generate OTP max limit is 9999
 * If number is 3 or 2 digits then add 0s before(prefix)
 * @author Shettem Raghavendra
 *
 */
public class TestOptGen {

	public static void main(String[] args) {
		int random = new Random().nextInt(9999);
		String otp = String.format("%04d", random);
		System.out.println(otp);
	}
}
