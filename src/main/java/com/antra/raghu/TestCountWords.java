package com.antra.raghu;
/**
 * Java Program to Count Number of Words in Given String
 * @author Shettem Raghavendra
 *
 */
public class TestCountWords {

	public static void main(String[] args) {
		String str = " Hello Fine OK Bye ";
		str = str.trim();
		//case#1
		System.out.println(str.split(" ").length);
		//case#2
		char[] chars = str.toCharArray();
		int count = 1;
		for(char c:chars) {
			if(c==' ')
				++count;
		}
		System.out.println(count);
		//case#3
		count = 1;
		for (int i = 0; i < str.length(); i++) {
			if(str.charAt(i)==' ')
				count++;
		}
		System.out.println(count);
	}
}
