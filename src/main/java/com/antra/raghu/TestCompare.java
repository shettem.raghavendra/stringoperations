package com.antra.raghu;

public class TestCompare {

	public static void main(String[] args) {
		String s1 = "Raghu";
		//new object is here
		String s2 = s1.toUpperCase();
		System.out.println(s1==s2);
		String s3 = "RAGHU";
		System.out.println(s2==s3);//comparing references, not hashcode
		System.out.println(s2.hashCode());
		System.out.println(s3.hashCode());
	}
}
