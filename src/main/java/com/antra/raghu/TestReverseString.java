package com.antra.raghu;

/**
 * Reverse A String Without Using Reverse Built In Method
 * @author Shettem Raghavendra
 *
 */
public class TestReverseString {

	public static void main(String[] args) {
		String str = "Hello! Users.";
		char arr[] = str.toCharArray();
		char temp = ' ';
		int arrLength = arr.length;
		for(int i=0;i<=arrLength/2;i++) {
			temp = arr[i];
			arr[i] = arr[arrLength-i-1];
			arr[arrLength-i-1] = temp;
		}
		str = new String(arr);
		System.out.println(str);
	}
}
